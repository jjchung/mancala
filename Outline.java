//uses circular linked list data structure
// 4 seeds in each pit except 1 & 8

class MancalaBoard {
    
    protected class Pits {
        protected int seeds;
        protected Pits next;
        protected int pitNum; // which pit?
        
        public Pits(int pitNum) {
            next = null;
            this.pitNum = pitNum;
            if (pitNum == 1 || pitNum == 8) {
                seeds = 0;
            } else {
                seeds = 4;
            }
        }
    }
    
    public MancalaBoard {
        private Pits first; // first pit
        
        public MancalaBoard() {
            first = new Pits(1);
        }
        
        public void addPit() {
            if (first == null) {
                first = new Pits(1);
            }
            Pits temp = first;
            while(temp.next != null) {
                temp = temp.next;
            }
            temp.next = new Pits(temp.pitNum + 1);
            temp.next.next = first;
        }
        
        public initializeBoard() {
            for (int i = 0; i < 14) {
                this.addPit();
            }
        }
        
        
    }
}